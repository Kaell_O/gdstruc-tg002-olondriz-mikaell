#include <iostream>

using namespace std;

int sum(int input)
{
	if (input == 0)
		return 0;

	return (input % 10 + sum(input / 10));
}

void fibonacci(int input, int i = 0, int first = 0, int second = 1, int sum = 0)
{
	if (i == 0)
	{
		cout << first << " ";
		fibonacci(input, i + 1);
	}

	else if (i == 1 && input > i)
	{
		cout << second << " ";
		fibonacci(input, i + 1);
	}

	else if (input > i)
	{
		sum = first + second;
		first = second;
		second = sum;

		cout << sum << " ";
		fibonacci(input, i + 1, first, second, sum);
	}
}

bool prime(int input, int i = 2)
{
	if (input <= 2)
		return true;
	if (input % i == 0)
		return false;
	if (i * i > input)
		return true;

	return prime(input, i + 1);
}


int main()
{
	int input;
	cin >> input;

	cout << "Sum of digits: " << sum(input) << endl;
	cout << "Prime? ";
	if (prime(input))
		cout << "Yes";
	else
		cout << "No";

	cout << '\n' << "Fibonacci: ";
	fibonacci(input);
}