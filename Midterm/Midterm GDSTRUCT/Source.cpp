#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"

#include <time.h>

using namespace std;

void printArrays(UnorderedArray<int>* array1, OrderedArray<int>* array2)
{
	cout << "Unordered contents: ";
	for (int i = 0; i < array1->getSize(); i++)
		cout << (*array1)[i] << "   ";
	cout << "\nOrdered contents: ";
	for (int i = 0; i < array2->getSize(); i++)
		cout << (*array2)[i] << "   ";
}

void main()
{
	srand(time(NULL));
	cout << "Enter size of dynamic array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}
	int input = -1;
	while (true)
	{
		system("CLS");
		printArrays(&unordered, &ordered);

		cout << "\n\nWhat do you want to do ?"
			<< "\n1 - Remove element at index"
			<< "\n2 - Search for element"
			<< "\n3 - Expand and generate random values" << endl;
		cin >> input;

		switch (input)
		{
			case 1:
			{
				cout << "Enter index to remove: ";
				cin >> input;

				unordered.remove(input);
				ordered.remove(input);
				cout << endl;

				printArrays(&unordered, &ordered);

				cout << endl << endl;

				system("pause");
				break;
			}
			case 2:
			{
				cout << "Enter element to search: ";
				cin >> input;
				cout << endl;
				int linResult = unordered.linearSearch(input);
				int binResult = ordered.binarySearch(input);
				if (linResult >= 0)
				{
					cout << "Linear Search took " << unordered.getComparsions() << " comparisons." << endl;
					cout << "Binray Search took " << ordered.getComparsions() << " comparisons." << endl;
					cout << "Element " << input << " found. ";
					cout << "Index " << linResult << " for Unordered Array, ";
					cout << "Index " << binResult << " for Ordered Array." << endl << endl;

					system("pause");
					break;
				}
				else
				{
					cout << "Element " << input << " not found." << endl << endl;
					system("pause");
					break;
				}
			}
			case 3:
			{
				cout << "Input size of expansion: " << endl;
				cin >> input;
				for (int i = 0; i < input; i++)
				{
					int rng = rand() % 101;
					unordered.push(rng);
					ordered.push(rng);
				}
				cout << "Arrays have been expanded" << endl;
				printArrays(&unordered, &ordered);

				cout << endl;
				system("pause");
				break;
			}
			default:
			{
				system("pause");
				continue;
			}
				
		}
		
	}	
}