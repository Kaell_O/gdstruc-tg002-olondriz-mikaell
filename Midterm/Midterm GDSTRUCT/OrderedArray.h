#pragma once
#include <assert.h>

using namespace std;
template<class T>
class OrderedArray
{
public:
	OrderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~OrderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		
		//your code goes after this line

		int position = mNumElements;
		for (int i = 0; i < mNumElements; i++)
		{
			if (value < mArray[i])
			{
				position = i;
				break;
			}			
		}
		for (int i = mMaxSize-2; i >= position; i--)
		{
			mArray[i+1] = mArray[i];
		}	
	
		mArray[position] = value;
		mNumElements++;
	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL);
		if (index >= mMaxSize)
		{
			cout << "\nIndex is more than the current size!";
			return;
		}

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual T & operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}


	virtual int binarySearch(T val)
	{
		int left = 0;
		int right = mMaxSize - 1;
		comparisons = 0;
	
		while (left <= right)
		{
			int mid = left + ((right - left) / 2);
			if (mArray[mid] == val)
			{
				comparisons++;
				return mid;
			}
				
			else if (val < mArray[mid])
				right = mid - 1;
			else
				left = mid + 1;

			comparisons++;
		}

		return -1;
	}

	virtual int getComparsions()
	{
		return comparisons;
	}

private:
	int comparisons;
	T * mArray;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T * temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

};