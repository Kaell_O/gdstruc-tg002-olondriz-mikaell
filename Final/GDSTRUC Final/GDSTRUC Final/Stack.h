#pragma once
#include <assert.h>

using namespace std;
template<class T>
class Stack
{
public:
	Stack(int size, int growBy = 1) : mStack(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mStack = new T[mMaxSize];
			memset(mStack, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~Stack()
	{
		if (mStack != NULL)
		{
			delete[] mStack;
			mStack = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mStack != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		for (int i = mMaxSize - 2; i >= 0; i--)
		{
			mStack[i + 1] = mStack[i];
		}

		mStack[0] = value;
		mNumElements++;
	}

	void pop()
	{
		if (mNumElements <= 0)
			return;

		for (int i = 0; i < mMaxSize - 1; i++)
			mStack[i] = mStack[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	void clear()
	{
		mNumElements = 0;
	}

	const T& top()
	{
		return mStack[0];
	}

	virtual T& operator[](int index)
	{
		assert(mStack != NULL && index < mNumElements);
		return mStack[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}

	void print()
	{
		for (int i = 0; i < mNumElements; i++)
		{
			cout << mStack[i] << endl;
		}
	}

private:
	T* mStack;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mStack, sizeof(T) * mMaxSize);

		delete[] mStack;
		mStack = temp;

		mMaxSize += mGrowSize;
		return true;
	}
};