#include <iostream>
#include <string>
#include "Stack.h";
#include "Queue.h";

using namespace std;

//Used UnorderedArray.h and modified it for Stack.h and Queue.h
int main()
{
	int size = 0;
	cout << "Enter size for element sets: ";
	cin >> size;
	Stack<int> stack(size);
	Queue<int> queue(size);

	while (true)
	{
		system("CLS");
		int input = 0;

		cout << "What do you want to do ?"
			<< "\n1 - Push elements"
			<< "\n2 - Pop elements"
			<< "\n3 - Print everything then empty set" << endl << endl;
		cin >> input;

		switch (input)
		{
			case 1:
			{
				cout << "Push Value: ";
				cin >> input;

				stack.push(input);
				queue.push(input);

				cout << "\nStack Top:" << endl;
				cout << stack.top() << endl;
				cout << "\nQueue Top:" << endl;
				cout << queue.top() << endl;
				break;
			}
			case 2:
			{
				stack.pop();
				queue.pop();
				cout << "\n\nYou have popped the front elements." << endl;

				break;
			}
			case 3:
			{
				cout << "\nStack:" << endl;
				stack.print();
				cout << "\nQueue:" << endl;
				queue.print();

				stack.clear();
				queue.clear();
				break;
			}
			default:
			{
				system("pause");
				continue;
			}
		}

		system("pause");
	}
}