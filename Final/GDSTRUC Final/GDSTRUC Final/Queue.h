#pragma once
#include <assert.h>

using namespace std;
template<class T>
class Queue
{
public:
	Queue(int size, int growBy = 1) : mQueue(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mQueue = new T[mMaxSize];
			memset(mQueue, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~Queue()
	{
		if (mQueue != NULL)
		{
			delete[] mQueue;
			mQueue = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mQueue != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		mQueue[mNumElements] = value;
		mNumElements++;
	}

	void pop()
	{
		if (mNumElements <= 0)
			return;

		for (int i = 0; i < mMaxSize - 1; i++)
			mQueue[i] = mQueue[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	void clear()
	{
		mNumElements = 0;
	}

	const T& top()
	{
		return mQueue[0];
	}

	virtual T& operator[](int index)
	{
		assert(mQueue != NULL && index < mNumElements);
		return mQueue[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}

	void print()
	{
		for (int i = 0; i < mNumElements; i++)
		{
			cout << mQueue[i] << endl;
		}
	}

private:
	T* mQueue;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T* temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mQueue, sizeof(T) * mMaxSize);

		delete[] mQueue;
		mQueue = temp;

		mMaxSize += mGrowSize;
		return true;
	}
};